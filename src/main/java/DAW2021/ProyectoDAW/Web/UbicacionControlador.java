package DAW2021.ProyectoDAW.Web;
//


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import DAW2021.ProyectoDAW.Logica.UbicacionServicio;
import DAW2021.ProyectoDAW.Modelo.Ubicacion;



@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/ubicacion")

//
public class UbicacionControlador {
	
	@Autowired
	private UbicacionServicio servicio;
	
	@GetMapping(value = "/{id}")
	public Ubicacion getUbicacion(@PathVariable(name = "id")Integer id){
		return servicio.getUbicacion(id);
	}
	
	@GetMapping
	public List<Ubicacion> listarTodos(){
		return servicio.listarTodos();
	}
	
	@PostMapping
	public void guardar(@RequestBody Ubicacion t){
		servicio.guardar(t);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public Ubicacion actualizar(@RequestBody Ubicacion t) {
		return servicio.actualizar(t);
	}
	
	@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
	public void eliminar(@PathVariable(name = "id") Integer id){
		servicio.eliminar(id);
	}
	
	@GetMapping(value = "/{calle}")
	public Page<Ubicacion> getUbicacion(@PathVariable(name = "calle")String calle, Pageable pagina){
		return servicio.getUbicacion(calle, pagina);
	}
	
	@GetMapping(params = {"calle"})
	public Page<Ubicacion> listarTodosFiltradoPorCalle(String calle, Pageable pagina){
		return servicio.ListarTodosPorCalle(calle, pagina);
	}

}
