package DAW2021.ProyectoDAW.Web;
//
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


//import DAW2021.ProyectoDAW.Logica.CategoriaServicio;
import DAW2021.ProyectoDAW.Modelo.Categoria;
import DAW2021.ProyectoDAW.Logica.CategoriaServicio;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/categorias")

public class CategoriaControlador {

	@Autowired(required=true)
	private CategoriaServicio servicio;
	
	@GetMapping(value = "/{id}")
	public Categoria getCategoria(@PathVariable(name = "id")Integer id){
		return servicio.getCategoria(id);
	}
	
	@GetMapping(value = "/{nombre}")
	public Page<Categoria> getCategoria(@PathVariable(name = "nombre")String nombre, Pageable pagina) {		
		return servicio.getCategoria(nombre, pagina);
	}
	
	@GetMapping
	public List<Categoria> listarTodos(){
		return servicio.listarTodas();
	}
	

	@GetMapping(params = {"nombre", "descripcion"})
	public Page<Categoria> findByNombreContainingIgnoreCaseAndDescripcionContainingIgnoreCase(String nombre, String descripcion, Pageable pagina) {		
		return servicio.findByNombreContainingIgnoreCaseAndDescripcionContainingIgnoreCase(nombre, descripcion, pagina);
	}
	
	@GetMapping(params = {"nombre"})
	public Page<Categoria> listarTodasFiltradoPorNombreyPorDescripcion(String nombre, Pageable pagina) {		
		return servicio.listarTodasFiltradoPorNombre(nombre, pagina);
	}
	
	@GetMapping(params = {"descripcion"})
	public Page<Categoria> listarTodasFiltradoPorDescripcion(String descripcion, Pageable pagina) {		
		return servicio.listarTodasFiltradoPorDescripcion(descripcion, pagina);
	}
	
	
	@PostMapping
	public void guardar(@RequestBody Categoria c){
		servicio.guardar(c);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Categoria actualizar(@RequestBody Categoria c, @PathVariable(name = "id") Integer id) {
		return servicio.actualizar(c);
	}
	@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
	public void eliminar(@PathVariable(name = "id") Integer id){
	servicio.eliminar(id);
	}
	
}
