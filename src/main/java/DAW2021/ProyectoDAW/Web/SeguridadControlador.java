package DAW2021.ProyectoDAW.Web;

import java.security.Principal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SeguridadControlador {

	@RequestMapping("/login")
	public Principal login(Principal usuario) {
		return usuario;
	}
}
