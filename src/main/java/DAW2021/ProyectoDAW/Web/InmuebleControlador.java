package DAW2021.ProyectoDAW.Web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import DAW2021.ProyectoDAW.Logica.InmuebleServicio;
import DAW2021.ProyectoDAW.Modelo.Inmueble;



@RestController
@RequestMapping("/inmueble")
public class InmuebleControlador {

	@Autowired
	private InmuebleServicio servicio;
	
	@GetMapping(value = "/{id}")
	public Inmueble getInmueble(@PathVariable(name = "id")Integer id){
		return servicio.getInmueble(id);
	}
	
	@GetMapping
	public List<Inmueble> listarTodos(){
		return servicio.listarTodos();
	}
	@PostMapping
	public void guardar(@RequestBody Inmueble i){
		servicio.guardar(i);
	}
//	@RequestMapping(method = RequestMethod.PUT)
//	public Inmueble actualizar(@RequestBody Inmueble t) {
//		return servicio.actualizar(t);
//	}
	
	@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
	public void eliminar(@PathVariable(name = "id") Integer id){
		servicio.eliminar(id);
	}
	@GetMapping(params = {"id"})
	public Page<Inmueble> listarTodosFiltradoPorId(int id, Pageable pagina){
		return servicio.ListarTodosPorId(id, pagina);
	}
}
