package DAW2021.ProyectoDAW.Modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor

public class Inmueble {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private int mtsCuadrados;
	private int cantHabitaciones;
	private int cantBaños;
	@ManyToOne
	private Categoria categorias;
	@ManyToOne
	private Ubicacion ubicacion;
	
	
	public Ubicacion getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}
	public Categoria getCategoria() {
		return categorias;
	}
	public void setCategoria(Categoria categorias) {
		this.categorias = categorias;
	}
	public int getMtsCuadrados() {
		return mtsCuadrados;
	}
	public void setMtsCuadrados(int mtsCuadrados) {
		this.mtsCuadrados = mtsCuadrados;
	}
	public int getCantHabitaciones() {
		return cantHabitaciones;
	}
	public void setCantHabitaciones(int cantHabitaciones) {
		this.cantHabitaciones = cantHabitaciones;
	}
	public int getCantBaños() {
		return cantBaños;
	}
	public void setCantBaños(int cantBaños) {
		this.cantBaños = cantBaños;
	}
	public Integer getId() {
		return id;
	}
	
	
}
