package DAW2021.ProyectoDAW.Modelo;
//

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import lombok.Data;
import lombok.NoArgsConstructor;
//
@Entity
@Data
@NoArgsConstructor
//
public class Ubicacion {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String calle;
	private Integer numeroCalle;
	private String localidad;
	private String provincia;
	private String pais;
	private String piso;
	private Integer numeroDepto;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public Integer  getNumeroCalle() {
		return numeroCalle;
	}
	public void setNumeroCalle(Integer  numeroCalle) {
		this.numeroCalle = numeroCalle;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getPiso() {
		return piso;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}
	public Integer getNumeroDepto() {
		return numeroDepto;
	}
	public void setNumeroDepto(Integer numeroDepto) {
		this.numeroDepto = numeroDepto;
	}

}
