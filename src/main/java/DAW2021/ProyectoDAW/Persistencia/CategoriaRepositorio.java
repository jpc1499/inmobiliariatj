package DAW2021.ProyectoDAW.Persistencia;
//
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DAW2021.ProyectoDAW.Modelo.Categoria;

@Repository
public interface CategoriaRepositorio extends JpaRepository<Categoria, Integer> {

	Page<Categoria> findByNombreContainingIgnoreCase(String nombre, Pageable pagina);
	
	Page<Categoria> findByDescripcionContainingIgnoreCase(String descripcion, Pageable pagina);
	
	Page<Categoria> findByNombreContainingIgnoreCaseAndDescripcionContainingIgnoreCase(String nombre, String descripcion, Pageable pagina);

	
}
