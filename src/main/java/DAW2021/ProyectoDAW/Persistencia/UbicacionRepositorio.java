package DAW2021.ProyectoDAW.Persistencia;
//
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import DAW2021.ProyectoDAW.Modelo.Ubicacion;
//
@Repository
public interface UbicacionRepositorio extends JpaRepository<Ubicacion, Integer>{

	
//	List<Ubicacion> findByNombre(String calle);
//	List<Ubicacion> findByNombreIgnoreCase(String calle);
//	List<Ubicacion> findByNombreContainingIgnoreCaseOrderByNombre(String calle);
	Page<Ubicacion> findByCalleContainingIgnoreCase(String calle, Pageable pagina);
////	//numeroCalle
//	Page<Ubicacion> findByNumeroCalleContainingIgnoreCase(int numeroCalle, Pageable pagina);
////	//localidad
//	Page<Ubicacion> findByLocalidadContainingIgnoreCase(String localidad, Pageable pagina);
////	//provincia
//	Page<Ubicacion> findByProvinciaContainingIgnoreCase(String provincia, Pageable pagina);
////	//pais
//	Page<Ubicacion> findByPaisContainingIgnoreCase(String pais, Pageable pagina);
////	//pIso 
//	Page<Ubicacion> findByPisoContainingIgnoreCase(String piso, Pageable pagina);
////	//numeroDepto
//	Page<Ubicacion> findByNumeroDeptoContainingIgnoreCase(String numeroDepto, Pageable pagina);

	//@Query("select t from Ubicacion t")
	@Query("FROM Ubicacion t WHERE t.calle IN ?1")
	List<Ubicacion> buscarPorUbicacion(String calle);
//
//	
}
