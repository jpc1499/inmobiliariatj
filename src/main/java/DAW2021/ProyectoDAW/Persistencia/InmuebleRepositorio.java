package DAW2021.ProyectoDAW.Persistencia;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DAW2021.ProyectoDAW.Modelo.Inmueble;

@Repository
public interface InmuebleRepositorio extends JpaRepository<Inmueble, Integer> {

	Page<Inmueble> findByIdContaining(int id, Pageable pagina);
	Page<Inmueble> findByMtsCuadradosContaining(int mtsCuadrados, Pageable pagina);
	
}
