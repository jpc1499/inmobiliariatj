package DAW2021.ProyectoDAW.Logica;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import DAW2021.ProyectoDAW.Modelo.Inmueble;
import DAW2021.ProyectoDAW.Persistencia.InmuebleRepositorio;


@Service
public class InmuebleServicio {

	@Autowired
	private InmuebleRepositorio repositorio;

	public List<Inmueble> listarTodos(){
		Sort orden = Sort.by(Sort.Direction.ASC,"id");
		return repositorio.findAll(orden);
	}
	public Inmueble guardar(Inmueble i) {
		return repositorio.save(i);
	}
	
	public void eliminar(Integer id){
		repositorio.deleteById(id);
	}

	public Inmueble getInmueble(Integer id) {
		return repositorio.getOne(id);
	}
	 
	 
	public Page<Inmueble>ListarTodosPorId(int id , Pageable pagina){
		return repositorio.findByIdContaining(id, pagina);
	}
	
	public Page<Inmueble>ListarTodosPorMtsCuadrados(int mtsCuadrados, Pageable pagina){
		return repositorio.findByMtsCuadradosContaining(mtsCuadrados, pagina);
	}
	public InmuebleRepositorio getRepositorio() {
		return repositorio;
	}

	public void setRepositorio(InmuebleRepositorio repositorio) {
		this.repositorio = repositorio;
	}
	
	
	
	
	
	
	
//	public Inmueble actualizar(Inmueble t) {
//		if (t.getId()== null) {
//			throw new RuntimeException("ERROR NO TIENE ID");
//		}
//	}
}
