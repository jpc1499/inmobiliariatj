package DAW2021.ProyectoDAW.Logica;
//
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import DAW2021.ProyectoDAW.Persistencia.UbicacionRepositorio;
import DAW2021.ProyectoDAW.Modelo.Ubicacion;
//
@Service 
public class UbicacionServicio {

	@Autowired
	private UbicacionRepositorio repositorio;
	
	public List<Ubicacion> listarTodos(){
		Sort orden = Sort.by(Sort.Direction.ASC,"calle");
		return repositorio.findAll(orden);
	}
	
	public Ubicacion guardar(Ubicacion u) {
		return repositorio.save(u);
	}
	public Ubicacion actualizar(Ubicacion t) {
		if (t.getId()== null) {
			throw new RuntimeException("ERROR NO TIENE ID");
		}
		return repositorio.save(t);
	}
	public void eliminar(Integer id){
		repositorio.deleteById(id);
	}

	public Ubicacion getUbicacion(Integer id) {
		return repositorio.getOne(id);
	}
	 
	 
	public Page<Ubicacion>ListarTodosPorCalle(String calle , Pageable pagina){
		return repositorio.findByCalleContainingIgnoreCase(calle, pagina);
	}

	public Page<Ubicacion> getUbicacion(String calle, Pageable pagina) {
		return repositorio.findByCalleContainingIgnoreCase(calle, pagina);
	}
	
}
