package DAW2021.ProyectoDAW.Logica;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import DAW2021.ProyectoDAW.Persistencia.CategoriaRepositorio;
import DAW2021.ProyectoDAW.Modelo.Categoria;


@Service
public class CategoriaServicio {

	@Autowired
	private CategoriaRepositorio repositorio;
	
	public List<Categoria> listarTodas(){
		Sort order = Sort.by(Sort.Direction.ASC,"nombre","descripcion");
		return repositorio.findAll(order);
	}
	

	public Categoria guardar(Categoria c) {
		return repositorio.save(c);
	}
	
	
	public Categoria actualizar(Categoria c) {
		if (c.getId()== null) {
			throw new RuntimeException("ERROR NO TIENE ID");
		}
		return repositorio.save(c);
	}
	
	public void eliminar(Integer id){
		repositorio.deleteById(id);
	}
	public Categoria getCategoria(Integer id) {
		return repositorio.getOne(id);
	}

	public Page<Categoria> findByNombreContainingIgnoreCaseAndDescripcionContainingIgnoreCase(String nombre, String descripcion, Pageable pagina) {
		return repositorio.findByNombreContainingIgnoreCaseAndDescripcionContainingIgnoreCase(nombre, descripcion, pagina);
	}
	
	public Page<Categoria> listarTodasFiltradoPorNombre(String nombre, Pageable pagina) {
		return repositorio.findByNombreContainingIgnoreCase(nombre, pagina);
	}
	
	public Page<Categoria> listarTodasFiltradoPorDescripcion(String descripcion, Pageable pagina) {
		return repositorio.findByDescripcionContainingIgnoreCase(descripcion, pagina);
	}


	public Page<Categoria> getCategoria(String nombre, Pageable pagina) {
		return repositorio.findByNombreContainingIgnoreCase(nombre, pagina);
	}
	
}
