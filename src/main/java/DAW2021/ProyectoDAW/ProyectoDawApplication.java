package DAW2021.ProyectoDAW;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class ProyectoDawApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoDawApplication.class, args);
	}

}
